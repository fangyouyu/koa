const util = require('../../util')
module.exports = () => {
    return async (ctx,next) => {
        let path = ctx.path
        console.log(path)
        if(path != '/api/login' && path != '/api/register'){
            console.log(path)
            let authorization = ctx.request.headers.authorization
            let token = authorization? authorization.split('|')[1] : ''
            let res = await util.redis.get(authorization? authorization.split('|')[0] : '')
            if(res == token){
                await next();
            }else{
                ctx.body = util.json(0,{
                    msg:'token invalid'
                })
            }
        }else{
            await next();
        }
    }
}